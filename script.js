// Теоретические вопросы
// 1. Какие типы данных существуют в Javascript?
// string
// number
// bigInt
// boolean
// null
// symbol
// object
// undefined

// 2. В чем разница между == и ===?
// == грубое сравнение на равенство с приведением к одному типу
// === строгое сравнение сравнивает операнды на идентичность

// 3. Что такое оператор?
// Оператор это встроенная функция в язык, при его использовании вызывается то или инное действие и возвращает результат




let userName = prompt("Какое у вас имя?");

while ( (isNaN(userName) === false) || (userName == null) && (userName === "") ) {
    userName = prompt("Какое у вас имя?", userName);
}

let userAge = prompt("Какой у вас возраст?")
while( (isNaN(userAge)) || (userAge === null) && (userAge === "") ) {
    userAge = prompt("Какой у вас возраст?", userAge);
}

if(userAge < 18) {
    alert("You are not allowed to visit this website")
} else if (userAge >= 18 && userAge <= 22) {
    let confirmation = confirm("Are you sure you want to continue?");
    if (confirmation) {
        alert("Welcome, " + userName)
    }else {
        alert("You are not allowed to visit this website")
    }
} else {
    alert("Welcome, " + userName)
}